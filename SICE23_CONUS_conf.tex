%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\documentclass[fleqn,10pt,twocolumn]{SICE23}

\title{DDS over RPMsg on HMP\\
  A ROS2 system integration proposition for robotics}

\author{Vincent Conus${}^{1\dagger}$ Shinya Honda${}^{1}$ Ryota Kato${}^{2}$
  Yuki Muata${}^{2}$ Shinkichi Inagaki${}^{1}$ Tatsuya Suzuki${}^{3}$}

% The dagger symbol indicates the presenter.
\speaker{Vincent Conus}

\affils{${}^{1}$Department of Mechanical Engineering and System Control,
  Nanzan University, Nagoya, Japan\\
  (Tel: +81-80-4963-8257; E-mail: vincent.conus@pm.me)\\
  ${}^{2}$SHINMEI Industry Co., Ltd, Toyota, Japan\\
  ${}^{3}$Department of Mechanical System Engineering,
  Nagoya University, Nagoya, Japan
}

\abstract{
  As integration of multiple system on a singular chip become possible,
  Heterogeneous Multicore Processors (HMP) provide a way to have both general
  purpose cores running Linux and a real-time capable core on the same silicon,
  sharing memory and interfaces.
  This article presents the current state of the research, deployment and tests
  of the Robot Operating System 2 (ROS2) Data Distribution Service (DDS)
  communication system on abovementioned HMP processor, enabeling internal
  communication from Linux to the real time core running the Robot Operating
  System for microcontrollers (micro-ROS).
}

\keywords{
ROS2, micro-ROS, Linux, DDS, shared memory
}

% Strongly avoild hyphenation
\tolerance=1
\emergencystretch=\maxdimen
\hyphenpenalty=10000
\hbadness=10000

\begin{document}

\maketitle

%-------------------------------------------------------------------------------
% Comments and review from the draft
%-------------------------------------------------------------------------------
%
%
% In this paper, the evaluation of the developed system is
% lacked. The reviewers hoped that the authors would further
% demonstrate the performance of the developed system.
%
% - [X] Performance evaluation presented
% ------------------------------------------------------------------------------
%
% In this manuscript, you proposed the design concept of
% implementing ROS2 and micro-ROS on Heterogeneous Multicore
% Processors. Reviewer has some questions.
%
% You only described the abstract design concept of the robot
% control system. Therefore, it is difficult to evaluate the
% effectiveness of the proposed design concept. What indexes
% do you evaluate the effectiveness of this design concept?
% You should implement this system on SoC and describe the
% experimental results of the robot control.
%
% The following is a minor inquiry.
% - [X] Unneeded blanks and hyphens are included in some words.
% - [ ] There are no explanations in some abbreviated words.
%
% - [X] Indexes to evaluate the effectiveness of my design ?
%
% ------------------------------------------------------------------------------
%
% This paper presents the current status of research,
% deployment, and testing of the Robot Operating System 2
% (ROS2) Data Distribution Service (DDS) communication system
% on the HMP system, and proposes to enable internal
% communication with the shared real-time core of the Robot
% Operating System for microcontroller (micro- ROS).
% The combination of the proposed methods is very important
% for the future development of robots, and we look forward
% to its further development. We hope to see some
% demonstrations at the international conference. 


%-------------------------------------------------------------------------------
\section{Introduction}
%\label{sec:introduction}
A stronger integration of components for embedded system brings
notable advantages in performance and energy consumption\cite{goraczk2008energy}
for embedded systems.

In that regard, the use of HMP offers a way to integrate both general purpose
cores as well as real time capable ones on a single board.
This potential gain in performance and reduced part number, however, comes with
an increase in complexity for the firmware deployment.
Indeed, in the context of robotics systems development, one might want to be
running the ROS2 middleware on the general purpose, Linux capable cores, and
micro-ROS on the real time side, but the communication between both sides
should pass through the shared memory in order to take maximal advantage of
the stronger integration.

On the general purpose side, Linux allows the use of Remote Processor Framework
(remoteproc) to see the real time side as a device in it's file system;
and since Linux version 4.11, a Remote Processor Messaging (RPMsg) was
implemented, making it possible to send and receive messages between the
cores.

The goal of my project is to provide a way for ROS2 and micro-ROS, while
running on the same silicon chip, to exchange their DDS messages over RPMsg.

Implemented in robotics, this system should allow the control side to have a
more direct access to the peripheral devices without scarifying the true real
time capability of using a dedicate Real Time Operating System (RTOS)
compatible core.
The implementation presented here is using FreeRTOS\footnotemark[1] as it can
be used as a base to deploy micro-ROS.

\footnotetext[1]{FreeRTOS website: {www.freertos.org}}

% ------------------------------------------------------------------------------
\pagebreak
\section{ROS2 and micro-ROS for robotics}
%\label{sec:ros2-micro-ros}
In the context of robotic development and research, ROS2 and micro-ROS have
been utilized together\cite{mudalige2022hyperdog} but always on separated
hardware: a general-purpose processor unit for ROS2 and a real-time capable
micro-controller for micro-ROS.

\subsection{ROS2\protect\footnotemark[2]}
%\label{Asec:ros2}
A system provided by Open Robotics. Over the years, ROS became a widely used
open-source middleware for robotic development. Allowing hardware abstraction,
access to low-level devices and passing messages between processes running on
different pieces of hardware, ROS and it's most recent version ROS2 are beloved
and powerful tools with a variety of extension available.

ROS2's DDS publisher-subscriber communication system is a key part of this
project: a compatible agent in that network can subscribe to a topic and will
receive all the messages from it. This make the extension of the robotic system
with additional hardware very convenient. When a new agent is added to the
system, it can be reached by anyone given the appropriate topic is set.

\subsection{micro-ROS\protect\footnotemark[3]}
%\label{sec:micro-ros}
Micro-ROS is an effort supported by eProsima\footnotemark[4] to bring ROS2
capabilities to the micro-controller world and bridging the gap between
resource-constrained micro-controllers and larger
processors\cite{belsare2023microros}; most notably the access to DDS
communication as visible in the figure \ref{fig:micro-ros} below.


\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=6cm]{img/micro-ros}
    \caption{\label{fig:micro-ros} Software stack for a micro-ROS system,
      allowing communication between a ROS2 device (left) and a micro-ROS system
      (right), using the special ``DDS For Extremely Resource Constrained
      Environments'' (XRCE-DDS) Middleware layer to access the wider DDS
      network.}
  \end{center}
\end{figure}

\footnotetext[2]{ROS website: {www.ros.org}}
\footnotetext[3]{micro-ROS website: {micro.ros.org}}
\footnotetext[4]{eProsima website: {www.eprosima.com}}


% ------------------------------------------------------------------------------
\pagebreak
\section{Proposed architecture}
%\label{sec:prop-arch}
The current setup for using both ROS2 and micro-ROS for robotic system already
provides number of benefits, from unified communication to access to a
standardized platform.
However, in the context of an HMP, a strong integration of both system
would make more sense.
This section will present an overview of the architecture imagined for being
able to use micro-ROS on an HMP.


\subsection{RPMsg}
%\label{sec:rpmsg}
RPMsg is a key mechanic that should help to utilize the shared memory of an HMP
as a communication channel for ROS2's DDS. The figure \ref{rpmsg} below shows
the general principle on the Linux side.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=7cm]{img/rpmsg}
    \caption{\label{rpmsg} This schematic is a recreation of the proposed
      schematic from ST to present the inner working of
      RPMsg\protect\footnotemark[5].
      The RPMsg system allows to send and receive messages from a Linux user
      space (top), through the kernel space (center) using a device, to the
      remote processor, utilizing the shared memory (bottom).}
  \end{center}
\end{figure}

The RPMsg system appears as a character device (\verb|/dev/ttyRPMsg|), depending
on the \verb|remoteproc| system. The RPMsg will become available after a
handshake is performed with the co-processor, but also requires a kernel driver
to be available. It is possible to load such specific module for the application
automatically as the device starts, allowing the application to use this
communication channel directly.


\subsection{ROS2 deployment on HMP}
%\label{sec:deployment-hmp}
The figure \ref{general} shows the proposed evolution of a ROS2 and micro-ROS
system, from a situation with two separated devices, to a single HMP.\\

A variety of adaptations will be required in order to make this integration
possible. Even with a functional RPMsg communication channel, some modification
of both ROS2 and micro-ROS will be needed in order for the systems to understand
and use RPMsg as a protocol to transfer DDS messages.\\

Deploying ROS2 on a Linux platform is relatively straightforward, especially
since a package is available for Ubuntu-based distributions.
However, the deployment of micro-ROS on an HMP real-time side has never been
done and will require to create the support for the platform for micro-ROS.

Furthermore, the micro-ROS agent on the ROS2 side, visible earlier on the
figure \ref{fig:micro-ros}, will also need some modifications in order to be
able to use RPMsg.

The figure \ref{boards} hereafter shows both boards that were considered and on
which the ROS2 / micro-ROS deployment started.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=5.5cm]{img/general2}
    \caption{\label{general} DDS environment migrating onto a single device.\\
      The current situation (top) presents a typical micro-controller board
      communicating to a ROS2 system on different board through serial.
      Both systems are independant but the micro-controller might become limited
      by the reduced size of the available memory and slow serial port.\\
      The proposed evolution (bottom) shows both systems on a single chip:
      the DDS communication happens internally, over shared memory.
      In this case, both software stacks are sharing a common pool of memory.
      The FreeRTOS system potentially gains access to a larger amount of memory;
      and the integration of micro-ROS into the DDS system is accomplished,
      using a fast communication channel.}
  \end{center}
\end{figure}


\begin{figure}[ht]
  \begin{minipage}[c]{.4\linewidth}
    \includegraphics[width=\linewidth]{img/nitro}
  \end{minipage}\hfill
  \begin{minipage}[c]{.5\linewidth}
    \includegraphics[width=\linewidth]{img/kria}
  \end{minipage}
  \caption{\label{boards} The Nitrogen8M board\protect\footnotemark[6] (left)
    and the Kria KV260 board\protect\footnotemark[7] (right)
    are both running a similar general-purpose core (i.MX 8MQuad Core and Zynq
    UltraScale+ MPSoC, respectively) that support Linux as well as a real-time
    FreeRTOS on separated cores on they SoC. The development started on the
    Nitrogen8M board but eventually stopped and moved to the Kria, with
    encouraging results: building the micro-ROS functions static library was
    successful and could be integrated into the Kria board Cortex-R5F firmware.
    In both cases, it was possible to try the RPMsg communication system between
    the two types of cores. The challenge will be to utilize this communication
    layer as a custom transport for ROS2 DDS system.}
\end{figure}


\footnotetext[5]{ST wiki website about RPMsg:\hspace*{\fill}\\
  {wiki.st.com/stm32mpu/wiki/Linux\_RPMsg\_framework\_overview}}
\footnotetext[6]{Boundary's Nitrogen8m product page:\hspace*{\fill}\\
  {boundarydevices.com/product/nitrogen8m/}}
\footnotetext[7]{Xilinx's Kria KV260 product page:\hspace*{\fill}\\
  {www.xilinx.com/products/som/kria/kv260-vision-starter-kit.html}}

% ------------------------------------------------------------------------------
\pagebreak
\section{Goals \& evaluation}
%\label{sec:goals}
Implementing and deploying the ROS2 and micro-ROS system on a single chip will
require extensive testing and evaluation in order for the process to be
certified as being an effective and meaningful improvement over the current
state of the art of using these systems on separated hardware. This section will
present the elements that are set as important target for the system and that
will be evaluated in depth.\\

More importantly, here are the key points that must be addressed in the
system evaluation process of all the upcoming topics:

\begin{enumerate}
\item \textbf{Functionalities}:
  Does the functions implemented prior to the new system work as expected?
  Does the current functionalities implemented for the robot on ROS and ROS2
  still work with the new communication system?
\item \textbf{Stability}:
  Can the system sustain a long period of use? Can the system restart cleanly?
\item \textbf{Performance}:
  How does the new system compares to the previous / existing one?
  Is it significantly better?
\item \textbf{Usability}:
  Is the new implementation easily usable for robotic development?
  In the academic context, is it also usable by the students?
\end{enumerate}

\subsection{Tracing}
%\label{sec:tracing}
A key application of the described system would be to enable tracing and
debugging for ROS2 as proposed by C. Berard and al. in
2022\cite{bedard2022ros2tracing}, but with the additional integration of
micro-ROS devices.
The faster read / write access to this co-processor could enable to a more
detailed tracing system. This is particularly interesting since the
micro-controller in general are typically less convenient to trace and debug.
Such processes would often involve additional hardware (JTAG for example);
but with an heterogeneous system with shared memory, all the tracing could
potentially be done through the Linux side and be retrieved easily, even from
a network connection through the extensive ROS2 DDS network.

For such tracing system to be successful, the evaluation will consist first in
the implementation and trial for the functionalities, and then the evaluation of
the speed in which such tracing can be done. Such speed evaluation should be
compared to a similar system running on separated hardware and communicating
using serial.

It is to be noted that this tracing system for micro-ROS currently does not
exist at all. The implementation will thus benefit both standard serial setup
as well as our new RPMsg setup. Both implementation will also be required in
order to properly evaluate the RPMsg system performance.

\subsection{Points cloud and sensors access}
%\label{sec:points-cloud-sensors}
As the sensors, motors and actuators must be controlled and read in real time,
the connection to a micro-controller is required. With the usage of HMP, it
becomes possible to have a more direct access to these elements from the Linux
devices. As illustrated by the figure \ref{pointscloud}, the points cloud is a
key data for robot navigation.

This large amount of data should become available
to the real-time control, allowing an enhancement for both the ``reflex'' type
of control\cite{hosogaya} and but also the planning-based control, in this case
for the positioning of the feet of the six-legged robot.\cite{clawar2022}.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=6cm]{img/pointscloud}
    \caption{\label{pointscloud} A stereoscopic system on a robot allows to have
      a point mapping of the nearby environment.
      The amount of data is large and the access to it is key for the robot to
      have an accurate movement and positioning.
      Further hardware extension are under consideration, such as the use
      of both a stereo camera together with a depth sensor, making the
      access speed to such system a priority.}
  \end{center}
\end{figure}

\subsection{Overall robot control speed-up}
%\label{sec:over-robot-contr}
As a complement of both previous points, a general goal for this system
implementation into robots is to provide a significant gain in performance,
by accelerating the communication between the Linux and the real-time parts.
These gains should be evaluated with various methods: testing the communication
latency in both direction, comparing the data throughput compared to DDS over
serial, and also comparing electric power consumption.\\

The key factor to determine the success for this implementation will the
compatibility and improvement provided to the actual robot software as well as
the adaptations and new features that the hexapod robot team will have access
to. In general and as it was the case for the first point, the first evaluation
will be to test and compare the heterogeneous implementation to a more classic
multi-devices setup. This is based on the assumption that the bottleneck of the
current system is the serial communication between the boards, making the access
to the sensors and motor from the control system slow.\\

The faster communication should also allow to move some functionalities from the
micro-controller to the general-purpose core, providing more computing power for
these tasks.
This system can be implemented as a complement of the base acceleration of the
robot control system, while being also available if the result of the evaluation
of the performance increase by the new shared memory system alone is not
satisfactory.


% ------------------------------------------------------------------------------
\section{Conclusion}
%\label{sec:conclusion}
The research and development of a strong integration of the ROS ecosystem onto
a device with heterogeneous cores as presented in this paper should provide a
large amount of benefits for the development in robotics an beyond.\\

A fast communication between real-time and general purpose processors within the
same chip should provide the powerful Linux-based control side a more direct
access to the sensor data gathered by the real-time system. This should result
in an enhanced monitoring and debugging capability, as well as a greater
reliability due to the reduced number of parts involved.\\

The use of HMP for robots, especially in an academic context should provide a
greater degree of integration for a cost way lower than other custom solutions
such as FPGA or even custom chips. The processors in the HMP are standard
(Cortex A53 and Cortex M4 or Cortex R5F for the boards currently in test), but
their setup on a single silicon should be beneficial and bring significant
advantages as a ROS2 and micro-ROS implementation platform and as a base
system for robotic application development and research.\\

The testing, evaluation and tweaking of the system is going to be a key aspect
of the project, since this technology is only as valuable as the practical
benefits it can provide to the robot development world.
Factoring in the data throughput capability between both processing units but
also the stability and usability of the system is critical and I hope my work
will be a meaningful addition to the six-legged robot being worked on at our
laboratory, as well as for the ROS community as a whole.



% ------------------------------------------------------------------------------
%%%%%%%%%%%%%%%%% BIBLIOGRAPHY IN THE LaTeX file !!!!! %%%%%%%%%%%%%%%%%%%%%%%%%
%\pagebreak
\begin{thebibliography}{9}
\bibitem{goraczk2008energy}
  Goraczko Michel, Liu Jie, Lymberopoulos Dimitrios, Matic Slobodan,
  Priyantha Bodhi, Zhao Feng,
  ``Energy-optimal software partitioning in heterogeneous multiprocessor
  embedded systems'',
  {\it DAC '08: Proceedings of the 45th annual Design Automation Conference},
  pp. 191--196, 2008, DOI:10.1145/1391469.1391518.

\bibitem{bedard2022ros2tracing}
  Christophe Bédard, Ingo Lütkebohle, Michel Dagenais,
  ``ros2\_tracing: Multipurpose Low-Overhead Framework for Real-Time Tracing
  of ROS 2'',
  {\it IEEE Robotics and Automation Letters},
  pp. 6511--6518, 2022, DOI:10.1109/LRA.2022.3174346.

\bibitem{mudalige2022hyperdog}
  Nipun Dhananjaya Weerakkodi Mudalige, Iana Zhura, Ildar Babataev,
  Elena Nazarova, Aleksey Fedoseev, Dzmitry Tsetserukou,
  ``HyperDog: An Open-Source Quadruped Robot Platform Based on ROS2 and
  micro-ROS'',
  {\it 2022 IEEE International Conference on Systems, Man,
    and Cybernetics (SMC)}, 
  pp. 436--441, 2022, DOI:10.1109/SMC53654.2022.9945526.

\bibitem{hosogaya}
  H. Hosogaya, S. Inagaki, and T. Suzuki,
  ``Gait transition to spinning gait and fewer-legged walking for hexapod robot
  based on fcp gait control'',
  in 22nd IFAC World Congress, Yokohama, Japan, 2023, (to be appeared).

\bibitem{clawar2022}
  Tanada, K., Inagaki, S., Murata, Y., Kato, R., Suzuki, T. (2023),
  ``Semi-autonomous Walking Control of a Hexapod Robot Based on Contact Point
  Planning and Follow-the-Contact-Point Gait Control'',
  In: Cascalho, J.M., Tokhi, M.O., Silva, M.F., Mendes, A., Goher, K.,
  Funk, M. (eds),
  Robotics in Natural Settings. CLAWAR 2022.
  Lecture Notes in Networks and Systems, vol 530. Springer, Cham.
  doi.org/10.1007/978-3-031-15226-9\_28

\bibitem{belsare2023microros}
  Kaiwalya Belsare, Antonio Cuadros Rodriguez, Pablo Garrido Sánchez,
  Juanjo Hierro, Tomasz Kołcon, Ralph Lange, Ingo Lütkebohle, Alexandre Malki,
  Jaime Martin Losa, Francisco Melendez, Maria Merlan Rodriguez, Arne Nordmann,
  Jan Staschulat, and Julian von Mendel,
  ``Micro-ROS'',
  In: Anis Koubaa (ed.),
  {\it Robot Operating System (ROS): The Complete Reference (Volume 7),
    Springer,}
  pp. 3–55, 2023.
  
\end{thebibliography}

\end{document}

